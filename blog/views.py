from django.shortcuts import render
from .models import Blog


def index(request, **kwargs):
    q = request.GET.get('q')
    print(q)
    return render(request, 'index.html')

def show_gallery(request):
    blogs = Blog.objects.all()    
    return render(request, 'gallery.html', context={'blogs': blogs})
